let boxElement = document.getElementById('boxesContainer')
let hammer = new Hammer(boxElement)
//console.log(boxElement.children[1])
hammer.on("swipeleft swiperight", function(ev){
  let prevBoxIndex;
  for(let i=0; i<boxElement.children.length; i++){
    if(boxElement.children[i].classList.contains("disBlock")){
      boxElement.children[i].classList.remove("disBlock");
      boxElement.children[i].classList.add("disNone");
      prevBoxIndex = i;
    }
  }
  if(ev.type == "swipeleft"){
    if(prevBoxIndex < boxElement.children.length-1){
      boxElement.children[prevBoxIndex+1].classList.add("disBlock")
      boxElement.children[prevBoxIndex+1].classList.remove("disNone")
    }else{
      boxElement.children[0].classList.add("disBlock")
      boxElement.children[0].classList.remove("disNone")
    }
  }else{
    if(prevBoxIndex > 0){
      boxElement.children[prevBoxIndex-1].classList.add("disBlock")
      boxElement.children[prevBoxIndex-1].classList.remove("disNone")
    }else{
      boxElement.children[boxElement.children.length-1].classList.add("disBlock")
      boxElement.children[boxElement.children.length-1].classList.remove("disNone")
    }
  }
});
