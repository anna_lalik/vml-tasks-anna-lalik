'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const babel = require('gulp-babel');
// add sass and autoprefixer
gulp.task('sass', function () {
  return gulp.src('./*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 5 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task('sass:watch', function () {
  gulp.watch('./*.scss', ['sass']);
});
//add babel
gulp.task('babel', () =>
    gulp.src('file.js')
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(gulp.dest('dist'))
);

gulp.task('babel:watch', function(){
  gulp.watch('./*.js', ['babel'])
})

gulp.task('watch', ['sass:watch', 'babel:watch']);
gulp.task('default', ['sass', 'babel'])
