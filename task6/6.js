let arrayA = [1, 2, 4, 6, 11, 13, 17, 8, 20, 32, 33, 42, 47, 53, 59];
let arrayB = [11, 17, 13, 5, 53, 8, 2, 35, 47, 84, 1, 65, 151];
let arrayC = [];
let arrayD = [];
let notPrimeArray = [];


function isPrime(number) {
  if (number < 2) {
    return false;
  }
  for (let i = 2; i < number; i++) {
    if (number % i == 0) {
      return false
    }
  }
  return true
}

function bubbleSort(array) {
  for (let i = 0; i < array.length; i++) {
    if (array[i] > array[i + 1]) {
      let a = array[i];
      let b = array[i + 1];
      array[i] = b
      array[i + 1] = a
    }
  }
  return array
}

function nearestNotPrime(arr) {
  let nextNumber = 0;
  for (let i = 0; i < arr.length; i++) {
    nextNumber = arr[i] + 1;
    while (isPrime(nextNumber) == true) {
      nextNumber += 1
    }
    notPrimeArray.push(nextNumber)

  }
  return notPrimeArray;
}

function findDuplicatesPrimes(array1, array2) {
  let duplicates = [];
  let uniques = [];
  let array1Prime = [];
  let array2Prime = [];

  for (let i = 0; i < array1.length; i++) {
    if (isPrime(array1[i])) {
      array1Prime.push(array1[i])
    }
  }

  for (let j = 0; j < array2.length; j++) {
    if (isPrime(array2[j])) {
      array2Prime.push(array2[j])
    }
  }

  for (let k = 0; k < array1Prime.length; k++) {
    for (let l = 0; l < array2Prime.length; l++) {
      if (array1Prime[k] == array2Prime[l]) {
        duplicates.push(array1Prime[k])
      } else {
        uniques.push(array1Prime[k])
      }
    }
  }

  let duplicatesSorted = bubbleSort(duplicates)
  let theNearestNotPrimeArray = nearestNotPrime(duplicatesSorted)
  let result = {
    duplicatesSorted,
    theNearestNotPrimeArray
  }

  return result

}

console.log(findDuplicatesPrimes(arrayA, arrayB))
