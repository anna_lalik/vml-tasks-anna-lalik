function flattenArray(array){
  return array.reduce(function(accumulator, value){
    if(Array.isArray(value)){
      return accumulator.concat(flattenArray(value))
    }else{
      return accumulator.concat(value)
    }
  },[])
}
console.log(flattenArray([[1,2,[3]],4]))
